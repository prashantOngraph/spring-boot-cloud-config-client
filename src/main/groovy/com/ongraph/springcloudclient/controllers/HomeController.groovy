package com.ongraph.springcloudclient.controllers

import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RefreshScope
@RestController
@RequestMapping("/home")
class HomeController {

    @Value('${msg:Config Server is not working. Please check...}')
    private String message
/*
    msg property has been defined in client-config.properties
    msg property has been defined in client-config-development.properties
    msg property has been defined in client-config-production.properties

*/

    @GetMapping(value = "/index")
    String index() {
        return this.message
    }
}
