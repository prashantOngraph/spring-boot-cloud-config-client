package com.ongraph.springcloudclient

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.core.env.Environment

@SpringBootApplication
class SpringCloudClientApplication {

    static void main(String[] args) {
        SpringApplication.run(SpringCloudClientApplication, args)
    }

    @Autowired
    void setEnv(Environment e) {
        System.out.println("Property msg in client-config-${e.activeProfiles}.properties : " + e.getProperty("msg"));
    }

}
