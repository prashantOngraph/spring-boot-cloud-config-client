Overview

# **Spring Cloud Config**

Externalizing properties/resource file out of project codebase to an external service without
re-deploying any microservices.

**How to use it?**

1. Clone _https://gitlab.com/prashantOngraph/spring-boot-cloud-config_
This repository is containing external configuration which can be used by any client.
It contains 3 properties file according to environment.

2. Clone _https://gitlab.com/prashantOngraph/spring-boot-cloud-config-server_
This repository is **spring cloud server** which client interacts with to access configuration properties.

3. Clone _https://gitlab.com/prashantOngraph/spring-boot-cloud-config-client_
This repository is **spring cloud client** which invoked spring server for accessing
configuration properties.

4. Run spring-boot-cloud-config-server project

5. Run spring-boot-cloud-config-client
(The both should be run parallelly)

6. Invoke _http://localhost:8080/home/index_ in browser
It will show ("Hello Ongraph - Spring Boot Cloud Config POC for config server made by Prashant for - Development Environment") in browser.

6. Change 'msg' property in _**client-config-development.properties**_ under the spring-boot-cloud-config project

7. Commit change and push

7. Now use postman and hit _http://localhost:8080/actuator/refresh_ this API with _**POST**_ request
You will be able to see below response
`[
    "msg",
    "config.client.version"
]`

8. Now invoke again _http://localhost:8080/home/index_ in browser
You will see message you changed in **client-config-development.properties** under the spring-boot-cloud-config project.


